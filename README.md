<link rel="stylesheet" type="text/css" href="auto-number-title.css" />

LambdaChip Document
===================
Hardware License CC-BY-SA  

Schematic  
[LambdaChip Alonzo Schematic](https://gitlab.com/lambdachip/alonzo_schematic "LambdaChip Alonzo Schematic")  

Alonzo  
![LambdaChip Alonzo](images/_MG_4246_transparent_small.png "LambdaChip Alonzo")  

Saruman  
![LambdaChip Saruman](images/_MG_4248_transparent_small.png "LambdaChip Saruman")  

## Features<br/>
The LambdaChip Alonzo board features an ARM Cortex-M4 based STM32F411CE MCU
with a wide range of connectivity support and configurations. Here are
some highlights of the LambdaChip Alonzo board:
* STM32 microcontroller in UFQFN48 package
* It comes out with these resources:
    * 2.54 Header connecters
    * Bluetooth 4.0 BLE
    * 1 RGB LED and 1 white LED: D6 and D5
    * 3 buttons: BOOT0, RST and 1 User Button
    * TF card slot, user can compile program to files in TF card, virtual machine will load program from TF card.

### Zephyr Features
The Zephyr lambdachip_alonzo board configuration supports the following hardware features:
    * GPIO, TIMER, UART, SPI, I2C
    * detailed configurations are in the file
    *lambdachip-zephyr/lambdachip_alonzo/lambdachip_alonzo_defconfig*

Default Zephyr Peripheral Mapping:

    * UART_1 TX/RX          : PB9/PA10
    * UART_2 TX/RX          : PA2/PA3 (Connected to BLE chip)
    * I2C2 SCL/SDA          : PB10/PB3
    * I2C3 SCL/SDA          : PA8/PB4
    * SPI1 CS/SCK/MISO/MOSI : PA4/PA5/PA6/PA7 (Connected to TF card socket)
    * SPI2 CS/SCK/MISO/MOSI : PB12/PB13/PB14/PB15
    * User button           : PA1
    * LD2                   : PA5
    * TIM4 CH1/CH2/CH3/CH4  : PB6/PB7/PB8/PB9

### Pin description
![pin description](images/pins_description_20201231_1250_875.png "pin description")


### It has these parts
    [STM32F411CEU6](https://www.st.com/en/microcontrollers-microprocessors/stm32f411.html)

    * ARM Cortex-M4 MCU, 512KB flash, 128KB RAM, 100MHz, 1.7-3.6V, 36 GPIOs in UFQFN48 package
    * STM32F411CET6 in UFQFN48 package
    * ARM |reg| 32-bit Cortex |reg|-M4 CPU with FPU
    * 100 MHz max CPU frequency
    * VDD from 1.7 V to 3.6 V
    * 512 KB Flash
    * 128 KB SRAM
    * GPIO with external interrupt capability
    * 12-bit ADC with 16 channels, with FIFO and burst support
    * 8 General purpose timers
    * 2 watchdog timers (independent and window)
    * SysTick timer
    * USART/UART (3)
    * I2C (3)
    * SPI (5)
    * SDIO
    * USB 2.0 OTG FS
    * DMA Controller
    * CRC calculation unit


* BLE
    * FreqChip FR8016HA BLE chip
    [FR8016HA](https://newwezhanoss.oss-cn-hangzhou.aliyuncs.com/contents/sitefiles2038/10193999/files/200129..pdf)
* Li-ion Battery Charging Controller
    * [4056](http://www.tp4056.com/)
    * [4056 datasheet](http://www.tp4056.com/d/tp4056.pdf)
* Battery connector
    * 2.0mm edge battery socket
* TF card socket
    * User can compile the code into program.lef and copy to tf card, the program will load it automatically


### Description<br/>


## System requirements<br/>
Debian based GNU/Linux distributions
A TF card with FAT32 filesystem.

### Setting up Zephyr<br/>

[https://docs.zephyrproject.org/latest/getting_started/index.html](https://docs.zephyrproject.org/latest/getting_started/index.html)
```bash
west init ~/zephyrproject
cd ~/zephyrproject
# after setting up zephyr environment, these executables will be available
dtc
ninja
arm-none-eabi-gcc
arm-none-eabi-ld
west
```


### Development toolchains
* Debian GNU/Linux based distributions

*guile-3.0 won't work for now, since the data structure of record has changed*
*We'll fix them later.*
guile-2.2

```bash
sudo apt-get update
sudo apt-get install binutils-arm-none-eabi gcc-arm-none-eabi guile-2.2 libnewlib-arm-none-eabi libnewlib-dev libstdc++-arm-none-eabi-newlib gdb-multiarch

arm-none-eabi-gcc --version
# My gcc arm version:
# arm-none-eabi-gcc (15:8-2019-q3-1+b1) 8.3.1 20190703 (release) [gcc-8-branch revision 273027]
```

### LambdaChip environment
several projects are required
https://gitlab.com/lambdachip/lambdachip-bootloader-zephyr.git
https://gitlab.com/lambdachip/lambdachip-zephyr.git

```bash
cd ~
git clone https://gitlab.com/lambdachip/lambdachip-zephyr.git
git clone https://gitlab.com/lambdachip/lambdachip-bootloader-zephyr.git
cd ~/zephyrproject/zephyr/boards/arm
# create a soft link for the board description files.
ln -s ~/lambdachip-zephyr/lambdachip_alonzo ./lambdachip_alonzo

# build bootloader and project
mkdir -p ~/zephyrproject/build
west build -b lambdachip_alonzo -s ~/lambdachip-zephyr -d ~/zephyrproject/build/lambdachip-zephyr
west build -b lambdachip_alonzo -s ~/lambdachip-bootloader-zephyr -d ~/zephyrproject/build/lambdachip-bootloader-zephyr
```

### Burn firmware
When you get the PCB, it should have a bootloader on it. The bootloader is able to read the firmware fron Tf card and burn the firmware into MCU.
If the bootloader is on the MCU. Skip this step
```bash
# Burn the firmware with Saruman Debugger

ls /dev/ttyACM*
# /dev/ttyACM0  /dev/ttyACM1 should show up, if there are more serial ports, the index will increase accordingly.
gdb-multiarch
(gdb) set debug arm
(gdb) target extended-remote /dev/ttyACM0
# (gdb) ~/zephyrproject/build/lambdachip-bootloader-zephyr/zephyr/zephyr.elf
(gdb) file ~/zephyrproject/build/lambdachip-bootloader-zephyr/zephyr/zephyr.elf
# Reading symbols from ~/zephyrproject/build/lambdachip-bootloader-zephyr/zephyr/zephyr.elf...
(gdb) monitor swdp_scan # monitor jtag_scan
# Target voltage: 3.3V
# Available Targets:
# No. Att Driver
#  1      STM32F40x M4
(gdb) attach 1
# Attaching to program: ~/zephyrproject/build/lambdachip-bootloader-zephyr/zephyr/zephyr.elf, Remote target
(gdb) load
# Loading section rom_start, size 0x198 lma 0x8000000
# Loading section text, size 0x7692 lma 0x80001c0
# Loading section .ARM.exidx, size 0x8 lma 0x8007854
# Loading section initlevel, size 0xb8 lma 0x800785c
# Loading section sw_isr_table, size 0x2b0 lma 0x8007914
# Loading section rodata, size 0x7fc lma 0x8007bc4
# Loading section datas, size 0xe0 lma 0x80083c0
# Loading section devices, size 0xe4 lma 0x80084a0
# Loading section k_mem_slab_area, size 0x38 lma 0x8008584
# Start address 0x08001f50, load size 34194
# Transfer rate: 35 KB/sec, 814 bytes/write.
```
After these steps, the bootloader is already burnt into the MCU. Then copy the firmware to TF card
Copy the binary to TF card with the name *fimrware.bin*, only FAT32 format is allowed for the TF card.

```bash
cp ~/zephyrproject/build/lambdachip-zephyr/zephyr/zephyr.bin /media/r/TF_CARD/firmware.bin
sudo umount /media/r/TF_CARD
```
* Remaining Steps
    * Then unplug the TF card from your computer,
    * Unplug the USB Type-C cable
    * Plug the Type-C cable to LambdaChip Alonzo.
    * Wait for around 10 seconds. Then the upgrade will complete. The firmware.bin will be deleted.
    * If your serial port is linked to the computer, you can check the upgrade log

### Compile scheme code and let them run on LambdaChip Alonzo
install guile 2.2 or 2.3
```bash
sudo apt-get install guile-2.2
git clone https://gitlab.com/lambdachip/laco.git
cd laco
./configure
make -j3
```

Write the scheme program program.scm
```scheme
; program.scm
(define (main x)
    (gpio-toggle! "dev_led0")
    (usleep 200000)
    (if (= x 1)
        #t
        (main (- x 1))
        ))
(main 10)
```

Compile program.scm

```bash
./pre-inst-env scripts/laco program.scm
# this will compile program.scm to program.lef
```

Copy the program.lef to TF card and then insert the TF card to Alonzo

Then power on Alonzo, insert the USB Type-C cable and then you will see the LED blinking.

## End

